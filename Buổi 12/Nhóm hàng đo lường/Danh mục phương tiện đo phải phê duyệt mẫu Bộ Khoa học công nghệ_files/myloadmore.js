(function($){ // use jQuery code inside this to avoid "$ is not defined" error
  $(document).ready(function(){
    $('.hale_loadmore').click(function(){

      var button = $(this);


      $.ajax({ // you can also use $.post here
      	url : hale_loadmore_params.ajaxurl, // AJAX handler
      	data : {
       	action : 'loadmore',
       	query : hale_loadmore_params.posts, // that's how we get params from wp_localize_script() function
       	page : hale_loadmore_params.current_page,
       },
      	type : 'POST',
      	beforeSend : function ( xhr ) {
      		button.text('Loading...'); // change the button text, you can also add a preloader image
      	},
      	success : function( data ){
          //console.log(data);
      		if( data ) {
      			button.text( 'More posts' ).prev().before(data); // insert new posts
      			hale_loadmore_params.current_page++;
            // console.log(hale_loadmore_params.current_page);
            console.log(hale_loadmore_params.posts);
            // console.log(hale_loadmore_params.max_page);
      			if ( hale_loadmore_params.current_page == hale_loadmore_params.max_page )
      				button.remove(); // if last page, remove the button

      			// you can also fire the "post-load" event here if you use a plugin that requires it
      			// $( document.body ).trigger( 'post-load' );
      		} else {
      			// button.remove(); // if no data, remove the button as well
      		}
      	}
      });
    });
  });

})(jQuery);;
